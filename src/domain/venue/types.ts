export type VenueDataFields = {
  locationDescription: string;
  hasClothingStorage: boolean;
  hasSnackEatingPlace: boolean;
  outdoorActivity: boolean;
};
