import React from 'react';

import TextTitle from './TextTitle';

export default {
  title: 'TextTitle',
  component: TextTitle,
};

export const Default = () => <TextTitle>Text title</TextTitle>;
