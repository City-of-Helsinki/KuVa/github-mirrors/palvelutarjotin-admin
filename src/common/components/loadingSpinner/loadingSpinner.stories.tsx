import React from 'react';

import LoadingSpinner from './LoadingSpinner';

export default {
  title: 'LoadingSpinner',
  component: LoadingSpinner,
};

export const Loading = () => <LoadingSpinner isLoading />;
